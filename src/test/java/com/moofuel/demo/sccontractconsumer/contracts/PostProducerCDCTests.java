package com.moofuel.demo.sccontractconsumer.contracts;

import com.moofuel.demo.sccontractconsumer.domain.Post;
import com.moofuel.demo.sccontractconsumer.dto.Dto;
import com.moofuel.demo.sccontractconsumer.dto.IdDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Collection;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureStubRunner(
        ids = "${SC-CONTRACT-PRODUCER}",
        stubsMode = StubRunnerProperties.StubsMode.LOCAL)
public class PostProducerCDCTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    static {
        int port = ThreadLocalRandom.current().nextInt(10000, 65535);
        System.setProperty("SC-CONTRACT-PRODUCER", "com.moofuel.demo:sc-contract-producer:+:" + port);
        System.setProperty("posts.port", String.valueOf(port));
    }

    @Test
    public void findOneShouldReturnFoundPost() {
        final ResponseEntity<Post> response = this.testRestTemplate.getForEntity(
                "/posts/1",
                Post.class
        );

        assertThat(response.getStatusCode())
                .isEqualTo(HttpStatus.OK);
        final Post actual = response.getBody();
        assertThat(actual.getId())
                .isEqualTo(1);
    }

    @Test
    public void findAllShouldReturnCollectionOfPosts() {
        final ResponseEntity<Collection<Post>> response = this.testRestTemplate.exchange(
                "/posts",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Collection<Post>>() {
                }
        );
        assertThat(response.getStatusCode())
                .isEqualTo(HttpStatus.OK);
        assertThat(response.getBody())
                .isNotEmpty();
    }

    @Test
    public void createNewShouldReturnIdOfTheCreatedPost() {
        final Post post = new Post();
        post.setAuthor("kek");
        post.setContent("lol");
        post.setDate(new Date());
        post.setTitle("kekker");
        final ResponseEntity<IdDto> response = this.testRestTemplate.postForEntity(
                "/posts",
                post,
                IdDto.class
        );

        assertThat(response.getStatusCode())
                .isEqualTo(HttpStatus.CREATED);
        final IdDto actual = response.getBody();
        assertThat(actual.getId())
                .isInstanceOf(Number.class);
    }

    @Test
    public void updateExistingShouldSuccessfullyUpdateById() {
        final Post post = new Post();
        post.setTitle("title");
        post.setDate(new Date());
        post.setContent("content");
        post.setAuthor("author");
        post.setId(2L);
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        final ResponseEntity<Post> response = this.testRestTemplate.exchange(
                "/posts/2",
                HttpMethod.PUT,
                new HttpEntity<>(post, headers),
                Post.class);
        assertThat(response.getStatusCode())
                .isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()) //date не сравниваем, т.к. контракт генерит рандом по регулярке
                .isEqualToComparingOnlyGivenFields(post, "title", "content", "author", "id");
    }

    @Test
    public void deleteExistingShouldSuccessfullyDeleteById() {
        final ResponseEntity<Void> response = this.testRestTemplate.exchange("/posts/1",
                HttpMethod.DELETE,
                null,
                Void.class);
        assertThat(response.getStatusCode())
                .isEqualTo(HttpStatus.OK);
    }

    @Test
    public void uploadPicToPostById() {
        final LinkedMultiValueMap<String, Object> request = new LinkedMultiValueMap<>();
        request.add("pic", new ByteArrayResource("content".getBytes()) {
            @Override
            public String getFilename() {
                return "filename1.jpg";
            }
        });
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        final ResponseEntity<Dto> response = this.testRestTemplate.postForEntity(
                "/posts/1/pic",
                new HttpEntity<MultiValueMap<String, Object>>(request, httpHeaders),
                Dto.class
        );
        assertThat(response.getStatusCode())
                .isEqualTo(HttpStatus.OK);
        assertThat(response.getBody())
                .isEqualTo(new Dto(1L, "filename1.jpg"));
    }
}
