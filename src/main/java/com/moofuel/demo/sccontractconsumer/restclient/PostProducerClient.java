package com.moofuel.demo.sccontractconsumer.restclient;

import com.moofuel.demo.sccontractconsumer.domain.Post;
import com.moofuel.demo.sccontractconsumer.dto.Dto;
import com.moofuel.demo.sccontractconsumer.dto.IdDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collection;

public interface PostProducerClient {

    Collection<Post> findAll();

    Post findOne(Long id);

    IdDto createNewPost(Post post);

    Post updateExisting(Long id, Post post);

    void deleteExisting(Long id);

    Dto uploadPicToPost(Long id, MultipartFile pic) throws IOException;
}
