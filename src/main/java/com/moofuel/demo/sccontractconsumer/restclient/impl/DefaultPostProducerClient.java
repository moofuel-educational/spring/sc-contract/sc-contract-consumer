package com.moofuel.demo.sccontractconsumer.restclient.impl;

import com.moofuel.demo.sccontractconsumer.domain.Post;
import com.moofuel.demo.sccontractconsumer.dto.Dto;
import com.moofuel.demo.sccontractconsumer.dto.IdDto;
import com.moofuel.demo.sccontractconsumer.dto.PageSimpleDto;
import com.moofuel.demo.sccontractconsumer.properties.PostProducerClientProperties;
import com.moofuel.demo.sccontractconsumer.restclient.PostProducerClient;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collection;

@Service
@EnableConfigurationProperties(value = PostProducerClientProperties.class)
public class DefaultPostProducerClient implements PostProducerClient {

    private final RestTemplate restTemplate;
    private final PostProducerClientProperties properties;

    public DefaultPostProducerClient(RestTemplateBuilder restTemplate,
                                     PostProducerClientProperties properties) {
        this.restTemplate = restTemplate.build();
        this.properties = properties;
    }

    /**
     * забираем из ответа с паджинацией только поле content для упрощения
     */
    @Override
    public Collection<Post> findAll() {
        final ResponseEntity<PageSimpleDto> response = this.restTemplate.getForEntity(
                makeBasePostProducerUrl(),
                PageSimpleDto.class
        );
        return response.getBody().getContent();
    }

    @Override
    public Post findOne(Long id) {
        return this.restTemplate.getForEntity(
                makeBasePostProducerUrlWithPathVariables(id),
                Post.class
        ).getBody();
    }

    @Override
    public IdDto createNewPost(Post post) {
        return this.restTemplate.postForEntity(
                makeBasePostProducerUrl(),
                post,
                IdDto.class
        ).getBody();
    }

    @Override
    public Post updateExisting(Long id, Post post) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return this.restTemplate.exchange(
                makeBasePostProducerUrlWithPathVariables(id),
                HttpMethod.PUT,
                new HttpEntity<>(post, headers),
                Post.class
        ).getBody();
    }

    @Override
    public void deleteExisting(Long id) {
        this.restTemplate.delete(makeBasePostProducerUrlWithPathVariables(id));
    }

    @Override
    public Dto uploadPicToPost(Long id, MultipartFile pic) throws IOException {
        final LinkedMultiValueMap<String, Object> request = new LinkedMultiValueMap<>();
        request.add("pic", new ByteArrayResource(pic.getBytes()) {
            @Override
            public String getFilename() {
                return pic.getOriginalFilename();
            }
        });
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        return this.restTemplate.postForEntity(
                makeBasePostProducerUrlWithPathVariables(id, "pic"),
                new HttpEntity<MultiValueMap<String, Object>>(request, httpHeaders),
                Dto.class
        ).getBody();
    }

    private String makeBasePostProducerUrl() {
        return properties.getScheme()
                + "://" +
                properties.getHost()
                + ":" +
                properties.getPort()
                + "/" +
                properties.getPath();
    }

    private String makeBasePostProducerUrlWithPathVariables(Object... variables) {
        final StringBuilder baseUrl = new StringBuilder(makeBasePostProducerUrl());
        for (Object variable : variables) {
            baseUrl.append("/").append(variable);
        }
        return baseUrl.toString();
    }
}
