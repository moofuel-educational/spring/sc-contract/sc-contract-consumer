package com.moofuel.demo.sccontractconsumer.properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigurationProperties(prefix = "posts")
public class PostProducerClientProperties {

    private String scheme = "http";
    private String host;
    private int port = 8080;
    private String path = "posts";
}
