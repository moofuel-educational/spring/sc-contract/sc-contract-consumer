package com.moofuel.demo.sccontractconsumer.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Date;

@NoArgsConstructor
public class DateAsLongSerializer extends DateSerializer {
    @Override
    public void serialize(Date value, JsonGenerator g, SerializerProvider provider) throws IOException {
        g.writeNumber(value.getTime());
    }
}
