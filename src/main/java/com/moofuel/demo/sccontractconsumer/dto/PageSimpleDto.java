package com.moofuel.demo.sccontractconsumer.dto;

import com.moofuel.demo.sccontractconsumer.domain.Post;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageSimpleDto {
    private Collection<Post> content;
    private Sort sort;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class Sort {
        private Boolean sorted;
        private Boolean unsorted;
    }
}
