package com.moofuel.demo.sccontractconsumer.controlller;


import com.moofuel.demo.sccontractconsumer.domain.Post;
import com.moofuel.demo.sccontractconsumer.dto.Dto;
import com.moofuel.demo.sccontractconsumer.dto.IdDto;
import com.moofuel.demo.sccontractconsumer.restclient.PostProducerClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collection;

@RestController
@RequestMapping("/posts")
@RequiredArgsConstructor
@Slf4j
public class PostConsumerController {

    private final PostProducerClient postProducerClient;

    @GetMapping
    public Collection<Post> findAll() {
        return this.postProducerClient.findAll();
    }

    @GetMapping("/{id}")
    private Post findOne(@PathVariable Long id) {
        return this.postProducerClient.findOne(id);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public IdDto createNew(@RequestBody Post post) {
        return new IdDto(this.postProducerClient.createNewPost(post).getId());
    }

    @PutMapping("/{id}")
    public Post updateExisting(@PathVariable Long id,
                               @RequestBody Post post) {
        return this.postProducerClient.updateExisting(id, post);
    }

    @DeleteMapping("/{id}")
    public void deleteExisting(@PathVariable Long id) {
        this.postProducerClient.deleteExisting(id);
    }

    @PostMapping("/{id}/pic")
    public Dto uploadPicToPost(@PathVariable Long id,
                               @RequestBody MultipartFile pic) throws IOException {

        return postProducerClient.uploadPicToPost(id, pic);
    }
}
