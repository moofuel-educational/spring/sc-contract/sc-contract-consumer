package com.moofuel.demo.sccontractconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScContractConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScContractConsumerApplication.class, args);
    }
}
