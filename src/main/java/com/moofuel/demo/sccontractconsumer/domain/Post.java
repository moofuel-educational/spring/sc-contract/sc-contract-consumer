package com.moofuel.demo.sccontractconsumer.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.moofuel.demo.sccontractconsumer.jackson.DateAsLongSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    private Long id;
    @JsonSerialize(using = DateAsLongSerializer.class)
    private Date date;
    private String title;
    private String content;
    private String author;
}
